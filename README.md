# RemiOzcan_7_31082021

Backend: Node.js / SQL <br />
Frontend: Vue.js


## Description

Réseau social interne pour les employés de GROUPOMANIA, afin qu'il ecrivent et/ou partage des articles avec leurs collègues sur des sujet qui les intéressent.


## Installation
Depuis /backend: npm install <br />
Depuis /frontend/my-vue-cli-app: npm install <br />

## Démarrage
Server MySQL => changer info connection dans /backen/config/config.json
Depuis /backend: node server <br />
Depuis /frontend/my-vue-cli-app: npm run serve <br />
Comptes test : User1@gmail.com; User2@gmail.com  <br />
Compte admin : admin@admin.com <br/>
Mot de Passe de chaque compte : test123 <br/>



