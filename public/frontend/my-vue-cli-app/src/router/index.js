import Vue from 'vue'
import VueRouter from 'vue-router'
import Signin from '../views/Signin.vue'
import Home from '../views/Home.vue'
import Signup from '../views/Signup.vue'
import Newpost from '../views/Newpost.vue'
import Profil from '../views/Profil.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/newpost',
    name: 'Newpost',
    component: Newpost
  },

  {
    path: '/profil',
    name: 'Profil',
    component: Profil
  }

  
]

const router = new VueRouter({
  routes
})


export default router
