import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)
export const store = new Vuex.Store({
    state: {
      allMsg: [],
      allComments:[]
    },
    mutations: {
      videStore(state){
        state.allMsg = [];
        state.allComments = [];
      },
      changeDataMsg(state,messages){
        state.allMsg = [...messages];
      },
      changeDataComMsg(state,comments){
       state.allComments = [...comments];          
      }
    }
  })