const urlMsg = 'http://localhost:3000/';
const urlUser = 'http://localhost:3000/auth/';
const urlComment = 'http://localhost:3000/comment/';

export const API = {

//------------USER---------------------

  inscritpion: (data) => fetch(urlUser + 'signup',{
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
        },
      body: JSON.stringify(data), // Data = {email:,pseudo:,password:}
  })
  .then((response) => response.json())
  .then((data) => {
    return data;
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
  }),
    

  connexion: (data) => fetch(urlUser + 'signin',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
      },
    body: JSON.stringify(data), // Data = {email:,password:}
  })
  .then((response) => response.json())
  .then((data) => {
    return data;
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
  }),

  getOneUser: (idUser) => fetch(urlUser+idUser)
  .then(rawdata => rawdata.json())
  .then((data) => {
    return data.userFound;
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
  }),

  deleteUser: (idUser,token) => fetch(urlUser+idUser,{
    method: 'DELETE',
    headers: {
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    }
  }).then((response) => response.json())
  .then((data) => {
    return data;
  }),
  
//-------------MESSAGE---------------------

  createMsg:(data,token)  => fetch(urlMsg,{
    method: 'POST',
    headers: {
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data), // Data = {titre:,content:, userId:}
  })
  .then((response) => response.json())
  .then((data) => {
    return data;
  })
  //Then with the error genereted...
  .catch((error) => {
    console.error('Error:', error);
  }),

  getAllMsg: (token) => fetch(urlMsg,{
    method: 'GET',
    headers: {
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    }
  })
  .then(rawData => rawData.json()),

  getOneMsg: (idMsg,token) => fetch(urlMsg + idMsg,{
    headers:{'Authorization': 'Bearer '+ token,}
  })
  .then(rawData => rawData.json()),

  getAllMsgFromUser: (idUser,token) => fetch(urlMsg + 'me',{
    method: 'GET',
    headers: {
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(idUser)
  }),

  deleteMsg: (idMsg,idUser,token) => fetch(urlMsg + idMsg,{
    method: 'DELETE',
    headers: {
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(idUser)      
  })
  .then((response) => response.json()),

//----------COMMENT-----------

createCom: (data,token)  => fetch(urlComment,{
  method: 'POST',
  headers:{
      'Authorization': 'Bearer '+ token,
      'Content-Type': 'application/json',
    },
  body: JSON.stringify(data), // Data = {idMessage:,content:, idUser:}
})
.then((response) => response.json())
.then((data) => {
  return data;
})
.catch((error) => {
  console.error('Error:', error);
}),

getAllComs: (token) => fetch(urlComment ,{
  method: 'GET',
  headers:{
    'Authorization': 'Bearer '+ token,
    'Content-Type': 'application/json',
  }
})
.then(rawData => rawData.json())
.then(data =>{
  return data;
}),

getOneCom: (idCom) => fetch(urlComment + idCom)
.then(rawData => rawData.json()), //return?

getAllComFromUser: (idUser,token) => fetch(urlComment + 'me',{
  method: 'GET',
  headers: {
    'Authorization': 'Bearer '+ token,
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(idUser),
}),

deleteCom: (idCom,idUser,token) => fetch(urlComment + idCom,{
  method: 'DELETE',
  headers: {
    'Authorization': 'Bearer '+ token,
    'Content-Type': 'application/json',
  },
  body: JSON.stringify(idUser)
})
.then((response) => response.json())
    
};