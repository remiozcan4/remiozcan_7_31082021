const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/user');

router.post('/signup', userCtrl.signup);
router.post('/signin',userCtrl.login);
router.get('/:id', userCtrl.profil);
router.delete('/:id',userCtrl.efface);

module.exports = router;