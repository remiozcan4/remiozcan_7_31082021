const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');


const commentCtrl = require ('../controllers/comment');

router.get('/me',auth, commentCtrl.getAllFromUser);
router.get('/',auth,commentCtrl.getAllComs);
router.get('/:id',commentCtrl.getOne);
router.post('/', auth, commentCtrl.createCom);
router.delete('/:id',auth, commentCtrl.delete);

module.exports = router;