const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const messageCtrl = require ('../controllers/message');

router.get('/', auth, messageCtrl.getAll);
router.get('/me', auth, messageCtrl.getAllFromUser)
router.get('/:id',auth,messageCtrl.getOne);
router.post('/',auth, messageCtrl.createMsg);
router.delete('/:id',auth, messageCtrl.delete);

module.exports = router;