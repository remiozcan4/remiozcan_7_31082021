const {Message, User, Comment} = require('../models');


exports.getAllFromUser = (req, res) => {
    User.findOne({where:{id : req.body.idUser}})
    .then((userFound)=>{
        if(!userFound){
            return res.status(404).json({ error: 'Utilisateur non trouvé !' });
        }
    const idUser = userFound.id;
    Comment.findAll({where : {idUser : idUser}})
    .then((comments) => {
        if(comments.length == 0){
            return res.status(404).json({Message: 'Pas de commentaire posté.'});
        }
        res.status(200).json(comments)
    })
    .catch((error) => res.status(500).json(error));
    })
    .catch((error) => res.status(500).json(error));
}

exports.getOne = (req, res) => {
    Comment.findOne({ where: {id: req.params.id} })
    .then((comment) => {
        if(!comment){
            return res.status(404).json({error: 'Commentaire insexistant.'})
        }
        res.status(200).json(comment)
    })
    .catch(error => res.status(500).json(error));   
}

exports.getAllComs= (req, res) => {
    Comment.findAll()
    .then((comments) => {
        res.status(200).json(comments)
    })
    .catch((error) => res.status(500).json(error));
}

exports.createCom = (req, res) => {
   const idMessage = req.body.idMessage;
   const content = req.body.content;
   const userId = req.body.idUser;

   User.findOne({where : { id : userId} })
   .then((userFound) => {
       if(userFound){
           Message.findOne({where : { id : idMessage} })
           .then((messageFound) => {
                Comment.create ( {
                    MessageId : messageFound.id,
                    UserId : userFound.id,
                    content : content
                })
                .then((Comment) => res.status(201).json(Comment))
                .catch(error => res.status(404).json( error));   
            })
            .catch(() => res.status(404).json({error: 'Message inexistant'}));
           
       }
       else{
           res.status(404).json({error : 'Utilisateur introuvable'})
       }
   })
   .catch(error => res.status(500).json({erreur: error}))

}

exports.delete = (req, res) => {
    const idComSupp = req.params.id;
    const idUserConnected = req.body.idUser;
    Comment.findOne({ where: {id: idComSupp} })
    .then((comment) => {
        User.findOne({ where: { id : idUserConnected} })
        .then((userFound) => {
            if(comment.UserId === userFound.id || userFound.role === 'admin'){
                Comment.destroy({where: { id : comment.id}})
                .then(res.status(200).json('commentaire supprimé'))
                .catch(()=> res.status(500).json({error: 'impossible de supprimer le commentaire'}))
            }
            else{
                return res.status(401).json('Autorisation refusée');
            }
        })
        .catch(error => res.status(500).json({erreur: error}));
        
    })
    .catch(error => res.status(500).json(error));

}
