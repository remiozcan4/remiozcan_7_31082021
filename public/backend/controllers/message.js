const {Message, User, Comment} = require('../models');


exports.getAll = (req, res) => {
    Message.findAll()
    .then((messages) => res.status(200).json(messages))
    .catch((error) => res.status(404).json(error));
}

exports.getOne = (req, res) => {
    Message.findOne({ where: {id: req.params.id} })
    .then((message) => {
        if(!message){
            return res.status(404).json({ error: 'Message non trouvé !' });
        }
        res.status(200).json(message)
    })
    .catch(error => res.status(500).json( error));   
}

exports.getAllFromUser =(req, res) => {
    User.findOne({where:{id : req.body.idUser}})
    .then((userFound)=>{
        if(!userFound){
            return res.status(404).json({ error: 'Utilisateur non trouvé !' });
        }
        Message.findAll({where : { UserId : userFound.id}})
        .then((messages) => res.status(200).json(messages))
        .catch(error => res.status(404).json(error));
    })
    .catch(error => res.status(500).json( error));   
}

exports.createMsg = (req, res) => {
   const titre = req.body.titre;
   const content = req.body.content;
   const userId = req.body.idUser;

   User.findOne({where : { id : userId} })
   .then((userFound) => {
       if(userFound){
           Message.create ( {
               UserId : userFound.id,
               titre : titre,
               content : content
           })
           .then((message) => res.status(200).json(message))
           .catch(error => res.status(404).json( error));   
       }
       else{
           res.status(404).json({error : 'Utilisateur introuvable'})
       }
   })
   .catch(error => res.status(500).json({erreur: error}))

}

exports.delete = (req, res) => {
    const idMessageSupp = req.params.id;
    const idUserConnected = req.body.idUser;
    Message.findOne({ where: { id : idMessageSupp} })
    .then((message) => {
        User.findOne({ where: { id : idUserConnected} })
        .then((userFound) => {
            if(message.UserId === userFound.id || userFound.role === 'admin'){
                Message.destroy({ where : {id : idMessageSupp} })
                .then(res.status(200).json('post supprimé'))
                .catch(error => json({erreur : error})); 
            }
            else{
                return res.status(401).json('Autorisation refusée');
            }
        })
        .catch(error => res.status(500).json({erreur: error}));
    })
    .catch(error => res.status(500).json( {erreur: error}));

}

