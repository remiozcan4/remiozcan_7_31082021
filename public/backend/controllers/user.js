const {Message, User, Comment} = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.signup = (req, res) => {
  User.findOne({ where: { email: req.body.email}})
    .then((user) => res.status(400).json({error: 'Un compte avec comme email '+ user.email+ ' existe déjà.'}))
    .catch(() => {
      const mail = req.body.email;
      const password = req.body.password;
      const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const regexMdp =  /^.*(?=.{6,})(?=.*\d).*$/;
      if(regexMail.test(String(mail).toLocaleLowerCase())){
        if(regexMdp.test(String(password).toLocaleLowerCase())){
          bcrypt.hash(req.body.password,10)
          .then(hash =>{
            User.create({
              email: mail,
              pseudo: req.body.pseudo,
              password:hash,
            })
            .then((user) => res.status(201).json({ userId: user.id+ 'e utilisateur créé'}))
            .catch(error => res.status(400).json({ error }));
          })
          .catch(error => res.status(500).json({ error }))
        }else{
          return res.status(400).json({error : "Veuillez rentrer un Mot de Passe Valide (au moins 6 caractères et un numéro)"})
        }
      }else{
        return res.status(400).json({error: "Veuillez rentrer un e-mail valide."})
      }
     
    });  
};

exports.login = (req, res) => {
  User.findOne({ where: { email: req.body.email}})
    .then(user => {
      if(!user){
        return res.status(400).json({ error: 'E-mail ou mot de passe incorrect ! '});
      }
      bcrypt.compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            return res.status(401).json({ error: 'E-mail ou mot de passe incorrect ! ' });
          }
          res.status(200).json({ 
            user: user.id,
            role:user.role,
            token: jwt.sign(
              { userId: user.id },
              process.env.TOKENKEY,
              { expiresIn: '2h' })
          });  
        })
        .catch(error => res.status(500).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
}  


exports.efface = (req,res) => {
  User.findOne({where:{id : req.params.id}})
  .then((userFound)=>{
    if(!userFound){
      return res.status(404).json({ error: 'Utilisateur non trouvé !' });
    }
    User.destroy({where:{id : userFound.id}})
        .then( () => res.status(200).json({message:'Utilisateur supprimé'}))
        .catch(() => res.status(500).json({message: 'impossible de supprimer l utilisateur'})); 
  })
  .catch(error => res.status(500).json({ error }));
}

exports.profil = (req, res) => {
  User.findOne({ where: {id: req.params.id} })
  .then((user) => {
    if(!user){
      return res.status(404).json({ error: 'Utilisateur non trouvé !' });
    }
      delete user.password;
      res.status(200).json({ userFound: user})
  })
  .catch(error => res.status(500).json({ error }));
}
