const { Sequelize } = require('sequelize');
const express = require('express');
const app = express();
const userRoutes = require('./routes/user');
const commentRoutes = require('./routes/comment');
const messageRoutes = require('./routes/message');
require('dotenv').config();

const db = require("./models");
db.sequelize.sync();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use(express.json());
app.use(express.urlencoded({ extended: true }));




app.use('/auth',userRoutes);
app.use('/comment',commentRoutes);
app.use('/',messageRoutes);

app.use((req,res)=> {
    res.status(404).send('404 : not found')
})


module.exports = app;
