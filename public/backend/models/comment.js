module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define('Comment', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        unique: true,
        allowNull: false,
        autoIncrement: true
    },
    content: {
        type: DataTypes.STRING,
        unique: true,
    },
  },
  {
      freezeTableName :true

});
  Comment.associate = (models) => {
    Comment.belongsTo(models.Message,{
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: false
    }
    })
    Comment.belongsTo(models.User, {
      onDelete: 'CASCADE',
      foreignKey: {
        allowNull: false
    }
    });
};

return Comment;
}
  