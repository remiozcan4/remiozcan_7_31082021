module.exports = (sequelize, DataTypes) => {
    const Message = sequelize.define('Message', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            allowNull: false,
            autoIncrement: true
        },
        titre: {
            type: DataTypes.STRING,
            allowNull: false
        },
        content: {
            type: DataTypes.STRING,
            allowNull: false
        },
      },
      {
          freezeTableName :true
  
    });
       Message.associate = (models) => {
        Message.hasMany(models.Comment)
        Message.belongsTo(models.User,{
            onDelete: 'CASCADE',
            foreignKey: {
                allowNull: false
            }
        })
}

return Message
  };
  