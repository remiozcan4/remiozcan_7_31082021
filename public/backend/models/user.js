module.exports = (sequelize, DataTypes) => {
  
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            unique: true,
            allowNull: false,
            autoIncrement: true
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            isEmail: true,
            unique: true,
        },
        pseudo : {
            type: DataTypes.STRING,
            allowNull: false
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        },
        role: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: "user"
        },
      },{
          freezeTableName :true
  
    });

    User.associate = (models) => {
      User.hasMany(models.Message)
      User.hasMany(models.Comment)
  };

  return User;
};
  

